return {
	{
		"nvim-tree/nvim-tree.lua",
		dependencies = {
			"nvim-tree/nvim-web-devicons",
		},
		config = function()
			require("nvim-tree").setup({
				sort_by = "case_sensitive",
				view = {
					width = 35,
				},
				renderer = {
					group_empty = true,
				},
				filters = {
					dotfiles = false,
				}, 
			})
			vim.keymap.set("n", "<C-n>", require("nvim-tree.api").tree.toggle, { desc = "Toggle NvimTree" })
		end
	}
}
