return {
	{
		"nvim-treesitter/nvim-treesitter",
		event = { "BufReadPost", "BufNewFile" },
		main = "nvim-treesitter.configs",
		build = ":TSUpdate",
		dependencies = {
			"nvim-treesitter/nvim-treesitter-textobjects",
			{ "nvim-treesitter/nvim-treesitter-context", opts = { enable = false } },
			"JoosepAlviste/nvim-ts-context-commentstring",
			"RRethy/nvim-treesitter-endwise",
			"windwp/nvim-ts-autotag",
			"andymass/vim-matchup",
		},
		cmd = {
			"TSUpdate",
			"TSInstall",
			"TSInstallInfo",
			"TSModuleInfo",
			"TSConfigInfo",
			"TSUpdateSync",
		},
		keys = {
			{ "v", desc = "Increment selection", mode = "x" },
			{ "V", desc = "Shrink selection", mode = "x" },
		},
		---@type TSConfig
		---@diagnostic disable-next-line: missing-fields
		opts = {
			highlight = { enable = true },
			indent = { enable = true },
			refactor = {
				highlight_definitions = { enable = true },
				highlight_current_scope = { enable = true },
			},

			-- See: https://github.com/RRethy/nvim-treesitter-endwise
			endwise = { enable = true },

			-- See: https://github.com/andymass/vim-matchup
			matchup = {
				enable = true,
				include_match_words = true,
			},

			-- See: https://github.com/windwp/nvim-ts-autotag
			autotag = {
				enable = true,
				filetypes = {
					"html",
					"javascript",
					"javascriptreact",
					"jsx",
					"tsx",
					"typescript",
					"typescriptreact",
					"vue",
				},
			},

			-- See: https://github.com/JoosepAlviste/nvim-ts-context-commentstring
			context_commentstring = { enable = true, enable_autocmd = false },

			incremental_selection = {
				enable = true,
				keymaps = {
					init_selection = false,
					node_incremental = "v",
					scope_incremental = false,
					node_decremental = "V",
				},
			},

			-- See: https://github.com/nvim-treesitter/nvim-treesitter-textobjects
			textobjects = {
				select = {
					enable = true,
					lookahead = true,
					keymaps = {
						["af"] = "@function.outer",
						["if"] = "@function.inner",
						["ac"] = "@class.outer",
						["ic"] = "@class.inner",
						["a,"] = "@parameter.outer",
						["i,"] = "@parameter.inner",
					},
				},
				move = {
					enable = true,
					set_jumps = true,
					goto_next_start = {
						["],"] = "@parameter.inner",
					},
					goto_previous_start = {
						["[,"] = "@parameter.inner",
					},
				},
				swap = {
					enable = true,
					swap_next = {
						[">,"] = "@parameter.inner",
					},
					swap_previous = {
						["<,"] = "@parameter.inner",
					},
				},
			},

			-- https://github.com/nvim-treesitter/nvim-treesitter#supported-languages
			ensure_installed = {
				"bash",
				"comment",
				"css",
				"diff",
				"git_config",
				"git_rebase",
				"gitcommit",
				"gitignore",
				"gitattributes",
				"html",
				"java",
				"kotlin",
				"lua",
				"luadoc",
				"luap",
				"make",
				"markdown",
				"markdown_inline",
				"regex",
				"sql",
				"terraform",
				"toml",
				"tsx",
				"typescript",
				"vim",
				"vimdoc",
				"vue",
			},
		},
	},
}
