local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup("tco.plugins", {
	root = vim.fn.stdpath("data") .. "lazy",
	lockfile = vim.fn.stdpath("config") .. "lazy-lock.json",
	{
		defaults = {
			lazy = true,
		},
	},
	checker = {
		enabled = false,
	},
	change_detection = {
		enabled = true,
	},
})

vim.keymap.set("n", "<leader>lz", vim.cmd.Lazy, { desc = "Open Lazy (plugin manager)" })
