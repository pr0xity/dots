return {
	{
		"mhartington/formatter.nvim",
		config = function()
			require("formatter").setup({
				logging = true,
				log_level = vim.log.levels.WARN,
				filetype = {
					lua = {
						require("formatter.filetypes.lua").stylua,
					},
					kotlin = {
						require("formatter.filetypes.kotlin").ktlint,
					},
					["*"] = {
						require("formatter.filetypes.any").remove_trailing_whitespace,
					},
				},
			})

			vim.keymap.set( "n", "<leader>f", ":Format<CR>", { desc = "Formatting text using language specific formatter" })
			vim.keymap.set( "n", "<leader>F", ":FormatWrite<CR>", { desc = "Formatting text using language specific formatter" })
		end,
	},
}
