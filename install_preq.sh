#!/bin/sh
set -e
set -u

. ./variables.sh
. ./funcs.sh


if ! [ -d $HOME/.n ]; then
	if ! which npm &> /dev/null; then
		red "npm not found. installing ..."
		doinstall npm
	fi

	export N_PREFIX=$HOME/.n

	sudo npm i -g n
	n install lts

	if ! [ $(grep -ic "N_PREFIX" $HOME/.bashrc) -eq 1]; then
		echo 'export N_PREFIX="$HOME/.n"' >> $HOME/.bashrc
		echo 'export PATH="$HOME/.n/bin:$PATH"' >> $HOME/.bashrc
		green "added N_PREFIX to .bashrc"
	fi

	green "N (node version manager) finished installing ..."
	green "Remember to source .bashrc"
else
	green "N (node & npm) found ..."
fi

if ! command -v pip > /dev/null; then
	case $Distro in
		"Ubuntu")
			doinstall python3-pip
			;;
	esac
else
	green "pip found ..."
fi

if ! command -v fzf > /dev/null; then
	case $Distro in
		"Ubuntu")
			doinstall fzf
			;;
	esac
else
	green "fzf found ..."
fi

if ! command -v stylua > /dev/null; then
	wget https://github.com/JohnnyMorganz/StyLua/releases/download/v0.18.2/stylua-linux.zip && unzip stylua-linux.zip
	mv stylua $HOME/neovim/bin/
	green "StyLua installed ..."
else
	green "StyLua found ..."
fi

if ! command -v ktlint > /dev/null; then
	curl -sSLO https://github.com/pinterest/ktlint/releases/download/0.50.0/ktlint && chmod a+x ktlint && mv ktlint $HOME/neovim/bin
	green "ktlint installed ..."
else
	green "ktlint found ..."
fi

if ! command -v java > /dev/null; then
	wget https://download.java.net/java/GA/jdk20.0.2/6e380f22cbe7469fa75fb448bd903d8e/9/GPL/openjdk-20.0.2_linux-x64_bin.tar.gz
	tar xvf openjdk-*.tar.gz && cd jdk-*
	sudo cp -r * /usr/local/
	green "OpenJDK installed ..."
else
	green "OpenJDK found ..."
fi

if ! command -v kotlin > /dev/null; then
	wget https://github.com/JetBrains/kotlin/releases/download/v1.9.10/kotlin-compiler-1.9.10.zip && unzip kotlin-compiler*.zip
	unzip kotlin-compiler*.zip && cd kotlinc
	sudo cp -r * /usr/local/
	green "Kotlin installed ..."
else
	green "Kotlin found ..."
fi
