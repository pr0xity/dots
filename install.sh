#!/bin/sh
set -e
set -u

. ./variables.sh
. ./funcs.sh


# custom install
install_nvim () {
	git clone https://github.com/neovim/neovim
	cd neovim && make distclean && make CMAKE_BUILD_TYPE=RelWithDebInfo 
	rm -r build/  # clear the CMake cache
	make CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=$HOME/neovim"
	make install
	export PATH="$HOME/neovim/bin:$PATH"
	if ! [ $(grep -ic "^export PATH=\"\$HOME/neovim/bin:\$PATH" $HOME/.bashrc) -eq 1 ]; then
		echo 'export PATH="$HOME/neovim/bin:$PATH"' >> $CONF_FILE
		green "export for neovim added to .bashrc"
	else
		red "export for neovim exists"
	fi
	pip install pynvim
	npm i -g neovim
	green "Neovim installed"
}

install_sdk () {
	curl -s "https://get.sdkman.io" | bash
}

install_mn () {
	sdk install micronaut
}

export N_PREFIX=$HOME/.n

CURRENT_SHELL="$(basename "$SHELL")"
CONF_FILE="$HOME/.bashrc"

green "${Bold}[OS]${Norm}:\t ${OS}"
green "${Bold}[Distro]${Norm}:\t ${Distro}"
green "${Bold}[Shell]${Norm}:\t ${CURRENT_SHELL}"
normal "--------------------------"
check_preq node
check_preq npm
check_preq pip

## checking preq that is specific to Distro
case $Distro in
	"Ubuntu")
		doinstall ninja-build
		doinstall gettext
		doinstall cmake
		doinstall unzip
		doinstall curl
		doinstall zip
		;;
esac

# Checking if neovim is installed
doinstall nvim
linkdots nvim
