local function add_options(option_table)
	for key, value in pairs(option_table) do
		vim.o[key] = value
	end
end

add_options({
	hlsearch = true,
	number = true,
	relativenumber = true,
	clipboard = "unnamedplus",
	breakindent = true,
	undofile = true,
	ignorecase = true,
	smartcase = true,
	signcolumn = "yes",
	expandtab = true,
	tabstop = 4,
	softtabstop = 4,
	smartindent = true,
	shiftwidth = 4,
	wrap = false,
	termguicolors = true,
	mouse = "a",
	backup = false,
	writebackup = false,
	swapfile = false,
	updatetime = 250,
	timeout = true,
	timeoutlen = 300,
	shortmess = vim.o.shortmess .. "c",
	completeopt = "menuone,noselect",
	laststatus = 3,
	fileencoding = "utf-8",
	encoding = "utf-8",
	background = "dark",
	lazyredraw = true,
	scrolloff = 10,
	cmdheight = 0,
	cursorline = true,
	title = true,
	showtabline = 0,
	showcmd = false,
	ruler = false,
	numberwidth = 2,
	splitbelow = true,
	splitright = true,
	showmode = false,
	incsearch = true,
})

vim.opt.isfname:append("@-@")
vim.opt.shortmess:append("sI")
vim.opt.whichwrap:append("<>[]hl")
vim.opt.iskeyword:append("-")
vim.opt.fillchars = {
	foldopen = "",
	foldclose = "",
	fold = " ",
	foldsep = " ",
	diff = "╱",
	eob = " ",
}
