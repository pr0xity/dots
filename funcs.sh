# printing functions
green () {
	echo "${Gre}$(date +'%H:%M:%S') [${Bold}INFO]${Norm} $1${RCol}"
}

normal () {
	echo "${RCol}$(date +'%H:%M:%S') $1"
}

yellow () {
	echo "${Yel}$(date +'%H:%M:%S') [${Bold}WARNING]${Norm} $1${RCol}"
}

red () {
	echo "${Red}$(date +'%H:%M:%S') [${Bold}ERROR]${Norm} $1${RCol}"
}

panic () {
	echo "${Red}$(date +'%H:%M:%S') [${Bold}PANIC]${Norm} $1${RCol}"
	exit 1
}

# install functions
check_preq () {
	(command -v $1 > /dev/null && green "$1 found ...") ||
		panic "$1 not found, install before proceeding with installation"
}

doinstall () {
	(command -v $1 > /dev/null && green "$1 found ...") ||
		(yellow "$1 not found, installing ..." && (install_$1 || sudo apt install $1))
}

linkdots () {
	if ! [ -d ~/.config ]; then
		red ".config folder not found, creating ..."
		mkdir ~/.config
	fi

	file="$1"
	if [ ! -e ~/.config/$file -a ! -L ~/.config/$file ]; then
		yellow "$file not found, creating symlink ..." >&2
		ln -s $(pwd)/$file ~/.config/$file
	else
		green "Symlink for $file found, ignoring ..." >&2
	fi
}
