# colors used
Gre='\033[0;32m'
Yel='\033[1;33m'
Red='\033[0;31m'
RCol='\033[0m'

# text formatting
Bold=$(tput bold)
Norm=$(tput sgr0)

# needing some variables
OS="`uname`"
case $OS in
  'Linux')
    OS='Linux'
    ;;
  'FreeBSD')
    OS='FreeBSD'
    ;;
  'WindowsNT')
    OS='Windows'
    ;;
  'Darwin')
    OS='Mac'
    ;;
  'SunOS')
    OS='Solaris'
    ;;
  'AIX') ;;
  *) ;;
esac

Distro=""
if [ -f /etc/os-release ]; then
	. /etc/os-release

	case $ID in
		ubuntu)
			Distro="Ubuntu"
			#sudo apt update -y
			;;
		debian)
			Distro="Debian"
			;;
		centos)
			Distro="CentOS"
			;;
		fedora)
			Distro="Fedora"
			;;
		*)
			Distro="Unknown: $ID"
			;;
	esac
else
	panic "/etc/os-release not found."
fi
